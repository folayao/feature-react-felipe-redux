import React from 'react'

const NotFound = () => {
    return (
        <React.Fragment>
            <h1>No encontrado</h1>
        </React.Fragment>
        /* o con fragments   
        <>
        Asi.....
        </>
        */
      
    )
}

export default NotFound;