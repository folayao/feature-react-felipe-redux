import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from '../containers/Home.jsx'
import Login from '../containers/Login'
import Register from '../containers/Register'
import NotFound from '../containers/NotFound'
import Layout from '../components/Layout'

const App = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/Login" component={Login} />
                    <Route exact path='/Register' component={Register} />
                    <Route component={NotFound} /> /* Este "Pasa" por una ruta no establecida */
                </Switch>
            </Layout>
        </BrowserRouter>
    );
};
export default App;